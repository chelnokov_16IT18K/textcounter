package ru.chelnokov.task;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Класс, считающий В данном текстовом файле:
 * <ul>
 * <li>Количество строк</li>
 * <li>Количество слов</li>
 * <li>Количество символов с учётом пробелов</li>
 * <li>Количество символов без учёта пробелов</li>
 * </ul>
 */
public class TextCount {
    public static void main(String[] args) {
        int wordCounter = 0;//счётчик для слов
        int counterOfCharsWithSpaces = 0;//счётчик символов, включая пробелы
        int counterOfCharsWithoutSpaces = 0;//счётчик символов, не включая пробелы
        int stringCounter = 0;//счётчик строк документа
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src//ru//chelnokov//text/test.txt"))) {
            String text;//строка текста
            while ((text = bufferedReader.readLine()) != null) {
                stringCounter++;

                counterOfCharsWithoutSpaces = getCounterOfCharsWithoutSpaces(counterOfCharsWithoutSpaces, text);

                counterOfCharsWithSpaces = getCounterOfCharsWithSpaces(counterOfCharsWithSpaces, text);

                wordCounter = getWordCounter(wordCounter, text);
            }
            System.out.println("Количество слов в тексте: " + wordCounter);
            System.out.println("Количество символов, считая пробелы: " + counterOfCharsWithSpaces);
            System.out.println("Количество символов без пробелов: " + counterOfCharsWithoutSpaces);
            System.out.println("Количество строк: " + stringCounter );

        } catch (IOException e) {
            System.out.println("Что-то явно пошло не так :(");
        }
    }

    /**
     * Возвращает обновлённое значение после каждой итерации цикла/каждой данной строки.
     * Самым последним значением будет количество слов текста
     * @param text строка из файла
     * @return новое значение слов
     */
    private static int getWordCounter(int wordCounter, String text) {
        String[] words = text.split("\\s+");
        wordCounter = wordCounter + words.length;
        return wordCounter;
    }

    /**
     * Возвращает обновлённое значение после каждой итерации цикла/каждой данной строки.
     * Самым последним значением будет количество символов в тексте с учётом пробелов
     * @param text строка из файла
     * @return новое значение символов
     */
    private static int getCounterOfCharsWithSpaces(int counterOfCharsWithSpaces, String text) {
        counterOfCharsWithSpaces = counterOfCharsWithSpaces + text.length();
        return counterOfCharsWithSpaces;
    }

    /**
     * Возвращает обновлённое значение после каждой итерации цикла/каждой данной строки.
     * Самым последним значением будет количество символов в тексте без учёта пробелов
     * @param text строка из файла
     * @return количество символов в данном документе, не учитывая пробелы
     */
    private static int getCounterOfCharsWithoutSpaces(int counterOfCharsWithoutSpaces, String text) {
        List<String> withoutSpaces = new ArrayList<>(Arrays.asList(text.split("\\s*")));
        withoutSpaces.removeAll(Arrays.asList("", null));
        counterOfCharsWithoutSpaces = counterOfCharsWithoutSpaces + withoutSpaces.size();
        return counterOfCharsWithoutSpaces;
    }
}